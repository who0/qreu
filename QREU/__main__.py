import base45, flynn,zlib,sys,json

from PIL import Image
from pyzbar import pyzbar




if (len( sys.argv) < 1):
    print("Donne moi une image")
    exit(2)

def run():
 name=sys.argv[1]
 image   = Image.open(name)
 barcode = pyzbar.decode(image)[0].data
 dbase45  = base45.b45decode(barcode[4:])
 zbase45 = zlib.decompress(dbase45)
 typeData = flynn.decoder.loads(zbase45)
 Res = flynn.decoder.loads(typeData[1][2])
 print(json.dumps(Res,indent=1))




if (__name__=="__main__"):
  run()
